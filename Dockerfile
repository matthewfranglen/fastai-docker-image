FROM nvidia/cuda:9.2-base-ubuntu18.04

# See http://bugs.python.org/issue19846
ENV LANG C.UTF-8
LABEL com.nvidia.volumes.needed="nvidia_driver"

RUN echo "deb http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64 /" > /etc/apt/sources.list.d/nvidia-ml.list

RUN apt-get update
RUN apt-get install --yes software-properties-common

RUN add-apt-repository ppa:deadsnakes/ppa
RUN apt-get update
RUN apt-get install --yes \
        python3.7         \
        build-essential   \
        cmake             \
        git               \
        curl              \
        wget              \
        vim               \
        ca-certificates   \
        python-qt4        \
        libjpeg-dev       \
        zip               \
        unzip             \
        libpng-dev
RUN rm -rf /var/lib/apt/lists/*

ENV LD_LIBRARY_PATH /usr/local/nvidia/lib:/usr/local/nvidia/lib64
ENV PYTHON_VERSION=3.7

RUN wget -O /tmp/anaconda-installer.sh https://repo.anaconda.com/archive/Anaconda3-2018.12-Linux-x86_64.sh
RUN chmod 755 /tmp/anaconda-installer.sh
RUN /tmp/anaconda-installer.sh -b -p /opt/conda
RUN rm /tmp/anaconda-installer.sh

ENV PATH=$PATH:/opt/conda/bin
RUN conda update -n root conda
RUN conda update --all
RUN conda install -c pytorch -c fastai fastai

RUN mkdir /.local /.torch
RUN chmod 777 /.local /.torch

WORKDIR /project

ENTRYPOINT ["jupyter", "notebook", "--no-browser", "--ip", "0.0.0.0", "--NotebookApp.custom_display_url=http://localhost:8888/"]
