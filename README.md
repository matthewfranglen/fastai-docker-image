Fast AI Docker Image
--------------------

This builds a docker image that can be used to run fast ai code.

It uses CUDA 9.2 and is loosely based on [the paperspace image](https://github.com/Paperspace/fastai-docker/tree/master/fastai-v3).

This does not produce a small image.

### Building

```bash
docker build --tag fastai .
```

### Running

This will load the current folder into the container and start the notebook within it.

```bash
docker run --rm --volume "${PWD}:/project" --publish 8888:8888 --user $UID:$GID fastai
```
